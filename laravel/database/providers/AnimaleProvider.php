<?php

use Faker\Provider\Company;

class AnimaleProvider extends Company
{
    protected static $animales = [
      'escarabajo','erizo','perro','gato','tortuga','conejo','periquito','serpiente','oso','caballo','elefante','tigre','pato','oca','ganso','cisne','avestruz','flamenco','pez paiaso','león','pantera','lobo','aguila','pingüino','mono'
      ,'gorila','orangutan','delfín','tiburón','loro','gallina','lemur','pulpo',
    ];

    /**
     * @example 'Lawyer'
     */
    public function animales()
    {
        return static::randomElement(static::$animales);
    }

    /**
     * @example 'Group'
     */

}

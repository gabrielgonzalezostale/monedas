<?php

use Faker\Provider\Company;

class ProductoProvider extends Company
{
    protected static $producto = [
      "Cepillo","Escoba","Pienso","Comedero","Juguete","Escopeta","Huesos","Pelota",
      "Toallas","Correa","Agua","Cinturón","Herradura","Silla de montar","Leche","Fusta","Fregona",
      "Rascador","Abanico para pavos","Galletas","Jaula","Cama","Regadera","Ratones congelados",
      "Cartuchos","Espray","Bolso",
    ];
    protected static $marca = [
      "La perla","Hacendaño","Marcas Paco","PerritosMcDuff","Violet","Asdidas","PeriquitosJuan","Fuma","TomiHeiffefer"
      ,"Mike","Duff","McRonalds","McFurry","GrandesJamones","PeraBebé","PH","GL","HF","Susu","HxH","MigasFree","CorchoPan"
      ,"Elocuente","Shiroko","Orslok","JaggerMister","Muzska","AlexElJapo","Sortensia","BataBlanck",
    ];
    /**
     * @example 'Lawyer'
     */
    public function productos()
    {
        return static::randomElement(static::$producto);
    }
    public function marcas()
    {
        return static::randomElement(static::$marca);
    }

    /**
     * @example 'Group'
     */

}

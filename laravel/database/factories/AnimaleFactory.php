<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Animale;

$factory->define(Animale::class, function (Faker $faker) {
$faker->addProvider(new \AnimaleProvider($faker));
    return [
        'Tipo_de_Animal' => $faker->unique()->animales,
        'Precio' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 4),
        'stock' => $faker->randomDigit,
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Producto;
use Faker\Generator as Faker;

$factory->define(Producto::class, function (Faker $faker) {
$faker->addProvider(new \ProductoProvider($faker));
    return [
        'Tipo_de_Producto' => $faker->unique()->productos,
        'Precio' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 4),
        'stock' => $faker->randomDigit,
        'Marca' => $faker->marcas,
    ];
});

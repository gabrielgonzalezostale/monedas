<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Cliente;

$factory->define(Cliente::class, function (Faker $faker) {
$faker->addProvider(new \Faker\Provider\es_ES\Person($faker));
$faker->addProvider(new \Faker\Provider\es_ES\PhoneNumber($faker));
$faker->addProvider(new \Faker\Provider\en_US\Address($faker));
$faker->addProvider(new \Faker\Provider\en_US\Person($faker));


    return [
        'Nombre' => $faker->firstName,
        'Apellidos' => $faker->lastName,
        'DNI' => $faker->unique()->dni,
        'Telefono' => $faker->unique()->tollFreeNumber,
        'Direccion' => $faker->state,
        'Correo' => $faker->unique()->email,
    ];
});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticulotruequesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulotrueques', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('nombre');
            $table->string('tipo');
            $table->string('descripcion')->nullable();
            $table->string('condicion')->nullable();
            $table->string('epoca')->nullable();
            $table->integer('agno')->nullable();
            $table->string('nacionalidad');
            $table->string('artbuscados');
            $table->string('img');
            //0 si no esta publicado, 1 para publicar
            $table->boolean('flag')->default(0);
            $table->timestamps();

            //user_id es una clave foranea que hace referencia al id de la tabla usuarios
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulotrueques');
    }
}

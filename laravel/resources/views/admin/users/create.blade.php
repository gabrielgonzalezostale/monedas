@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Página del Administrador para Crear Usuarios

@endsection

@section('contenido')

<style media="screen">
.red {
  color: red;
}
</style>

<div class="row">
<div class="col-lg-4"></div>
<div class="col-lg-8">
  <form action="/admin/users" method="post">
    @csrf
    <div class="form-group">

      <div class="col-lg-6">
          <label class="lead" for="name">Nombre <span class="red">*</span></label>
          <input type="text" name="name"  placeholder="Nombre" class="form-control" required autocomplete="name">
      </div>

    </div>
    <div class="form-group">
      <div class="col-lg-6">
          <label class="lead" for="email">Email <span class="red">*</span></label>
          <input type="email" name="email" placeholder="Email" class="form-control" required autocomplete="email">
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-6">
          <label class="lead" for="password">Contraseña <span class="red">*</span></label>
          <input type="password" name="password" placeholder="Contraseña" class="form-control" required autocomplete="password">
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-6">
          <label class="lead" for="rol">Rol de Usuario</label>
          <select class="form-control" name="rol">
            <option value="1" selected>Usuario</option>
            <option value="2">Moderador</option>
            <option value="3">Administrador</option>
          </select>
      </div>
    </div>

    <div style="margin-left:20%;padding-bottom:4%">
      <button class="btn btn-primary" type="submit" name="enviar">Crear Usuario</button>
    </div>
  </form>
</div>
</div>

@endsection

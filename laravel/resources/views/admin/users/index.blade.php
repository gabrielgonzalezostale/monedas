@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Página Principal del Administrador para Gestionar Usuarios

@endsection

@section('contenido')

<table class="table">
    <tr class="table-primary">
      <th scope="col">Id</th>
      <th scope="col">Rol</th>
      <th scope="col">Nombre</th>
      <th scope="col">Email</th>
      <th scope="col">Creado</th>
      <th scope="col">Actualizado</th>
    </tr>
    @if ($users)
    @foreach ($users as $user)
    <tr class="table-secondary">
      <td>{{$user->id}}</td>
      <td>@switch($user->role_id)
      @case(1)
          Usuario
          @break
      @case(2)
          Moderador
          @break
      @case(3)
          Administrador
          @break
      @endswitch</td>
      <td><a href="{{route('users.edit',$user->id)}}">{{$user->name}}</a></td>
      <td>{{$user->email}}</td>
      <td>{{$user->created_at}}</td>
      <td>{{$user->updated_at}}</td>
    </tr>
    @endforeach
    @endif
</table>

@endsection

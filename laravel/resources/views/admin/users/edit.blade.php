@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Página del Administrador para Editar el Usuario: {{$users->name}}

@endsection

@section('contenido')

<style media="screen">
.red {
  color: red;
}
</style>

<!--Formulario para Actualizar-->

<div class="row">
<div class="col-lg-4"></div>
<div class="col-lg-8">
  <form action="/admin/users/{{$users->id}}" method="post">
    @csrf
    @method('PUT')
    <div class="form-group">
      <div class="col-lg-6">
          <label class="lead" for="name">Nombre Usuario <span class="red">*</span></label>
          <input type="text" name="name" value="{{$users->name}}" class="form-control" required autocomplete="name">
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-6">
          <label class="lead" for="email">Email <span class="red">*</span></label>
          <input type="email" name="email" value="{{$users->email}}" class="form-control" required autocomplete="email">
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-6">
          <label class="lead" for="rol">Rol de Usuario</label>
          <select class="form-control" name="rol">
            @switch($users->role_id)
            @case(1)
              <option value="1" selected>Usuario</option>
              <option value="2">Moderador</option>
              <option value="3">Administrador</option>
                @break
            @case(2)
              <option value="1">Usuario</option>
              <option value="2" selected>Moderador</option>
              <option value="3">Administrador</option>
                @break
            @case(3)
              <option value="1">Usuario</option>
              <option value="2">Moderador</option>
              <option value="3" selected>Administrador</option>
                @break
            @endswitch
          </select>
      </div>
    </div>
    <div style="margin-left:20%;padding-bottom:4%">
      <button class="btn btn-primary" type="submit" name="enviar">Actualizar Usuario {{$users->name}}</button>
    </div>
    </form>
@if(isset($datos->id))
    <form action="/perfil/{{$datos->id}}" method="post">
      @csrf
      @method('PUT')
    <div class="form-group">
    <div class="col-lg-6">
        <label class="lead" for="nombre">Nombre Perfil</label>
        <input type="text" name="nombre"  value="{{$datos->nombre}}" class="form-control">
    </div>
    </div>

    <div class="form-group">
    <div class="col-lg-6">
        <label class="lead" for="apellido1">Apellido 1</label>
        <input type="text" name="apellido1"  value="{{$datos->apellido1}}" class="form-control">
    </div>
    </div>

    <div class="form-group">
    <div class="col-lg-6">
        <label class="lead" for="apellido2">Apellido 2</label>
        <input type="text" name="apellido2"  value="{{$datos->apellido2}}" class="form-control">
    </div>
    </div>

    <div class="form-group">
    <div class="col-lg-6">
        <label class="lead" for="direccion">Dirección</label>
        <input type="text" name="direccion"  value="{{$datos->direccion}}" class="form-control">
    </div>
    </div>


    <div class="form-group">
    <div class="col-lg-6">
        <label class="lead" for="ciudad">Ciudad</label>
        <input type="text" name="ciudad"  value="{{$datos->ciudad}}" class="form-control">
    </div>
    </div>


    <div class="form-group">
    <div class="col-lg-6">
        <label class="lead" for="provincia">Provincia</label><br>
        <select name="provincia">
          <option value="{{$datos->provincia}}" selected>{{$datos->provincia}}</option>
          <option value='Álava'>Álava</option>
          <option value='Albacete'>Albacete</option>
          <option value='Alicante'>Alicante</option>
          <option value='Almería'>Almería</option>
          <option value='Asturias'>Asturias</option>
          <option value='Ávila'>Ávila</option>
          <option value='Badajoz'>Badajoz</option>
          <option value='Barcelona'>Barcelona</option>
          <option value='Burgos'>Burgos</option>
          <option value='Cáceres'>Cáceres</option>
          <option value='Cádiz'>Cádiz</option>
          <option value='Cantabria'>Cantabria</option>
          <option value='Castellón'>Castellón</option>
          <option value='Ceuta'>Ceuta</option>
          <option value='Ciudad Real'>Ciudad Real</option>
          <option value='Córdoba'>Córdoba</option>
          <option value='Cuenca'>Cuenca</option>
          <option value='Girona'>Girona</option>
          <option value='Las Palmas'>Las Palmas</option>
          <option value='Granada'>Granada</option>
          <option value='Guadalajara'>Guadalajara</option>
          <option value='Guipúzcoa'>Guipúzcoa</option>
          <option value='Huelva'>Huelva</option>
          <option value='Huesca'>Huesca</option>
          <option value='Illes Balears'>Illes Balears</option>
          <option value='Jaén'>Jaén</option>
          <option value='A Coruña'>A Coruña</option>
          <option value='La Rioja'>La Rioja</option>
          <option value='León'>León</option>
          <option value='Lleida'>Lleida</option>
          <option value='Lugo'>Lugo</option>
          <option value='Madrid'>Madrid</option>
          <option value='Málaga'>Málaga</option>
          <option value='Melilla'>Melilla</option>
          <option value='Murcia'>Murcia</option>
          <option value='Navarra'>Navarra</option>
          <option value='Ourense'>Ourense</option>
          <option value='Palencia'>Palencia</option>
          <option value='Pontevedra'>Pontevedra</option>
          <option value='Salamanca'>Salamanca</option>
          <option value='Segovia'>Segovia</option>
          <option value='Sevilla'>Sevilla</option>
          <option value='Soria'>Soria</option>
          <option value='Tarragona'>Tarragona</option>
          <option value='Santa Cruz de Tenerife'>Santa Cruz de Tenerife</option>
          <option value='Teruel'>Teruel</option>
          <option value='Toledo'>Toledo</option>
          <option value='Valencia/Valéncia'>Valencia/Valéncia</option>
          <option value='Valladolid'>Valladolid</option>
          <option value='Vizcaya'>Vizcaya</option>
          <option value='Zamora'>Zamora</option>
          <option value='Zaragoza'>Zaragoza</option>
        </select>
    </div>
    </div>

    <div class="form-group">
    <div class="col-lg-6">
        <label class="lead" for="CP">Codigo Postal</label>
        <input type="number" name="CP"  value="{{$datos->CP}}" class="form-control">
    </div>
    </div>

    <div class="form-group">
    <div class="col-lg-6">
        <label class="lead" for="telefono">Teléfono</label>
        <input type="number" name="telefono"  value="{{$datos->telefono}}" class="form-control">
    </div>
    </div>


    <div style="margin-left:20%;padding-bottom:4%">
      <button class="btn btn-primary" type="submit" name="enviar">Actualizar Perfil de {{$users->name}}</button>

    </div>
  </form>
  @endif

  <!--Formulario para Borrar-->
  <form action="/admin/users/{{$users->id}}" method="post">
    @csrf
    @method('DELETE')
    <button type="submit" name="enviar" class="btn btn-danger">Eliminar Usuario {{$users->name}}</button>
  </form>

</div>
</div>

@endsection

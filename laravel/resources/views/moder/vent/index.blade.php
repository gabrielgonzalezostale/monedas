@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Pagina del moderador para Ver el Estado de los Articulos de Venta

@endsection

@section('contenido')

<table class="table">
    <tr class="table-primary">
      <th scope="col">Nombre</th>
      <th scope="col">Tipo</th>
      <th scope="col">Descripción</th>
      <th scope="col">Condición</th>
      <th scope="col">Época</th>
      <th scope="col">Año</th>
      <th scope="col">Nacionalidad</th>
      <th scope="col">Precio</th>
      <th scope="col">Imagen</th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
    @if (isset($venta))
    @foreach ($venta as $ven)
    @if ($ven->flag == 0)
    <tr class="table-secondary">
      <td>{{$ven->nombre}}</td>
      <td>{{$ven->tipo}}</td>
      <td>{{$ven->descripcion}}</td>
      <td>{{$ven->condicion}}</td>
      <td>{{$ven->epoca}}</td>
      <td>{{$ven->agno}}</td>
      <td>{{$ven->nacionalidad}}</td>
      <td>{{$ven->precio}}</td>
      <td><img width="200px" height="200px" src="../img/{{$ven->img}}"/></td>
      <td>
        <form action="/moder/vent/{{$ven->id}}" method="post">
          @csrf
          @method('PUT')
          <button class="btn btn-primary" type="submit" name="enviar">Publicar Articulo</button>
        </form>
      </td>
      <td>
      <form action="/moder/vent/{{$ven->id}}" method="post">
        @csrf
        @method('DELETE')
        <button type="submit" name="enviar" class="btn btn-danger">Eliminar Articulo</button>
      </form>
      </td>
    </tr>
    @endif
    @endforeach
    @endif
</table>

@endsection

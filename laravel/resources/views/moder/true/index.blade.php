@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Pagina del moderador para Ver el Estado de los Articulos de Trueque

@endsection

@section('contenido')

<table class="table">
    <tr class="table-primary">
      <th scope="col">Nombre</th>
      <th scope="col">Tipo</th>
      <th scope="col">Descripción</th>
      <th scope="col">Condición</th>
      <th scope="col">Época</th>
      <th scope="col">Año</th>
      <th scope="col">Nacionalidad</th>
      <th scope="col">Articulos Buscados</th>
      <th scope="col">Imagen</th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
    @if (isset($trueque))
    @foreach ($trueque as $tru)
    @if ($tru->flag == 0)
    <tr class="table-secondary">
      <td>{{$tru->nombre}}</td>
      <td>{{$tru->tipo}}</td>
      <td>{{$tru->descripcion}}</td>
      <td>{{$tru->condicion}}</td>
      <td>{{$tru->epoca}}</td>
      <td>{{$tru->agno}}</td>
      <td>{{$tru->nacionalidad}}</td>
      <td>{{$tru->artbuscados}}</td>
      <td><img width="200px" height="200px" src="../img/{{$tru->img}}"/></td>
      <td>
        <form action="/moder/true/{{$tru->id}}" method="post">
          @csrf
          @method('PUT')
          <button class="btn btn-primary" type="submit" name="enviar">Publicar Articulo</button>
        </form>
      </td>
      <td>
      <form action="/moder/true/{{$tru->id}}" method="post">
        @csrf
        @method('DELETE')
        <button type="submit" name="enviar" class="btn btn-danger">Eliminar Articulo</button>
      </form>
      </td>
    </tr>
    @endif
    @endforeach
    @endif
</table>

@endsection

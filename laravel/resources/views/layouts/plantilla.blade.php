<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/temas/bootstrap.min.css">
        <link rel="stylesheet" href="../css/temas/bootstrap.min.css">
        <link rel="stylesheet" href="../../css/temas/bootstrap.min.css">
        <link rel="stylesheet" href="../../../css/temas/bootstrap.min.css">
    </head>
    <body>

      <!--HEADER-->
      @include('layouts.header')
      @yield('header')

      <!--MENU-->
      @include('layouts.navbar')
      @yield('menu')

      <!--TITULO-->
      <div class="card bg-Secondary mb-3">
        <div class="text-center">
          <div class="" style="margin-left: 200px;margin-right: 200px">
        <h2>@yield('titulo')</h2>
          </div>
        </div>
      </div>

      <!--CONTENIDO DE LA PAGINA-->
      <div class="contenedor row">
        <div class="col-md-2" name="publi1">
            <a style="float:right" class="mx-auto" href="https://www.silvercoinseurope.com/"><img width="200px" height="auto" src="/img/Silvercoinseurope.jpg" alt=""></a>
            <a class="mt-3" style="float:right" href="https://www.numismaticadracma.com/index.php"><img width="200px" height="auto" src="/img/d1.png" alt=""></a>
        </div>
        <div class="col-md-8">
        @yield('contenido')
        </div>
        <div class="col-md-2" name="publi2">
        </div>
      </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../../js/bootstrap.min.js"></script>
    </body>
</html>

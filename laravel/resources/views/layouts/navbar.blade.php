<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="" style="margin-left: 350px;margin-right: 200px">

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">

      <li class="nav-item" style="margin-right: 30px">
      <a class="navbar-brand" href="/">Principal</a>
      </li>

      <li class="nav-item" style="margin-right: 30px">
        <a class="navbar-brand" href="/publitrue">Articulos Trueque</a>
      </li>

      <li class="nav-item" style="margin-right: 30px">
        <a class="navbar-brand" href="/publivent">Articulos a la Venta</a>
      </li>
    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item" style="margin-right: 30px">
                <a class="navbar-brand" href="{{ route('login') }}">{{ __('Iniciar sesión') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="navbar-brand" href="{{ route('register') }}">{{ __('Registrar') }}</a>
                </li>
            @endif
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="navbar-brand dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <!--solo visible para administradores-->
                  @if (Auth::user()->EsAdmin())
                  <a class="dropdown-item" href="/admin/users/create">
                      {{ __('Crear Nuevo Usuario') }}
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="/admin/users">
                      {{ __('Gestionar Usuarios') }}
                  </a>
                  <div class="dropdown-divider"></div>
                  @endif

                  <!--visible para moderadores-->
                  @if (Auth::user()->EsMod())
                  <a class="dropdown-item" href="/moder/true">
                      {{ __('Publicar Articulos Trueque') }}
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="/moder/vent">
                      {{ __('Publicar Articulos Venta') }}
                  </a>
                  <div class="dropdown-divider"></div>
                  @endif

                  <!--solo visible para usuarios registrados-->
                  @if (Auth::check())
                  <a class="dropdown-item" href="/perfil">
                      {{ __('Perfil') }}
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="/trueque">
                      {{ __('Cambiar Articulos') }}
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="/venta">
                      {{ __('Vender Articulos') }}
                  </a>
                  <div class="dropdown-divider"></div>
                  @endif

                  <!--visible para todos-->
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        {{ __('Cerrar sesión') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>

  </div>
  </div>
</nav>

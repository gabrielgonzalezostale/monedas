@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Articulos a la Venta

@endsection

@section('contenido')


@if (isset($venta))
@foreach ($venta->chunk(3) as $vent)
<div class="row">
  @foreach($vent as $ven)
  @if ($ven->flag==1)
  <div class="col-md-4">
    <div class="card border-primary mb-3 mx-auto" style="max-width: 20rem;">
      <div class="card-header"><h3 class="text-center">{{$ven->nombre}}</h3></div>
      <div class="card-body">
        <div class="thumbnail">
          <img class="mx-auto d-block img-thumbnail" width="242px" height="auto" src="img/{{$ven->img}}"/>
          <div class="caption">
            <h3 class="text-center">Precio:  {{$ven->precio}} €</h3>
            <p>{{$ven->descripcion}}</p>
            <p><a href="{{route('publivent.edit',$ven->id)}}" class="btn btn-primary" role="button">Ver Más</a> <a href="{{route('compra.edit',$ven->id)}}" class="btn btn-outline-success" role="button">Comprar</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif
  @endforeach
</div>
@endforeach
@endif


@endsection

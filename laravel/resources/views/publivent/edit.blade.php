@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Articulo a la Venta

@endsection

@section('contenido')

<style type="text/css">

.zoom {
-webkit-transition: all 0.35s ease-in-out;
-moz-transition: all 0.35s ease-in-out;
transition: all 0.35s ease-in-out;
cursor: -webkit-zoom-in;
cursor: -moz-zoom-in;
cursor: zoom-in;
}

.zoom:hover,
.zoom:active,
.zoom:focus {
-ms-transform: scale(2);
-moz-transform: scale(2);
-webkit-transform: scale(2);
-o-transform: scale(2);
transform: scale(2);
position:relative;
z-index:100;
}

.DivName {cursor: pointer}
}

</style>

<div class="container mt-5">
  <div class="row">
    <div class="col-6">
      <img class="d-block img-thumbnail zoom DivName" max-width: "100%" height="auto" src="../../img/{{$venta->img}}"/>
      <h5>{{$venta->descripcion}}</h5>
    </div>
    <div class="col-6">
      <div class="">
        <h3>{{$venta->nombre}}</h3>
      </div>
      <div class="mt-4">
        <h3>{{$venta->precio}} €</h3>
      </div>
      <div class="mt-4">
        <h4 class="d-inline">Condición: </h4><p class="d-inline"> {{$venta->condicion}}</p>
      </div>
      <div class="mt-4">
        <h4 class="d-inline">Época: </h4><p class="d-inline">{{$venta->epoca}}</p>
      </div>
      <div class="mt-4">
        <h4 class="d-inline">Año: </h4><p class="d-inline">{{$venta->agno}}</p>
      </div>
      <div class="mt-4">
        <h4 class="d-inline">Nacionalidad: </h4><p class="d-inline">{{$venta->nacionalidad}}</p>
      </div>
      <div class="mt-4">
        <h4 class="d-inline">Tipo de Articulo: </h4><p class="d-inline">{{$venta->tipo}}</p>
      </div>
      <div class="mt-4">
        <a href="{{route('compra.edit',$venta->id)}}" class="btn btn-success btn-lg" role="button">Comprar</a>
      </div>
    </div>
  </div>

@endsection

@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Perfil de {{$users->name}}

@endsection

@section('contenido')

<table class="table">
    <tr class="table-primary">
      <th scope="col">nombre</th>
      <th scope="col">apellido1</th>
      <th scope="col">apellido2</th>
      <th scope="col">dirección</th>
      <th scope="col">ciudad</th>
      <th scope="col">provincia</th>
      <th scope="col">CP</th>
      <th scope="col">teléfono</th>
    </tr>
    @if ($datos)
    <tr class="table-secondary">
      <td>{{$datos->nombre}}</td>
      <td>{{$datos->apellido1}}</td>
      <td>{{$datos->apellido2}}</td>
      <td>{{$datos->direccion}}</td>
      <td>{{$datos->ciudad}}</td>
      <td>{{$datos->provincia}}</td>
      <td>{{$datos->CP}}</td>
      <td>{{$datos->telefono}}</td>
    </tr>

      <a href="{{route('perfil.edit',$users->id)}}"><button type="submit" name="enviar" class="btn btn-danger">Modificar Perfil</button></a>

    @endif
</table>

@endsection

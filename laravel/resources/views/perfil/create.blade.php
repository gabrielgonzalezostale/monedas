@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Crear Perfil de {{$users->name}}

@endsection

@section('contenido')

<div class="row">
<div class="col-lg-4"></div>
<div class="col-lg-8">
  <form action="/perfil" method="post">
    @csrf

    <div class="form-group">

      <div class="col-lg-6">
          <label class="lead" for="nombre">Nombre</label>
          <input type="text" name="nombre"  placeholder="Nombre" class="form-control">
      </div>
      <div class="col-lg-6">
          <label class="lead" for="apellido1">Apellido 1</label>
          <input type="text" name="apellido1"  placeholder="Primer Apellido" class="form-control">
      </div>
      <div class="col-lg-6">
          <label class="lead" for="apellido2">Apellido 2</label>
          <input type="text" name="apellido2"  placeholder="Segundo Apellido" class="form-control">
      </div>
      <div class="col-lg-6">
          <label class="lead" for="direccion">Dirección</label>
          <input type="text" name="direccion"  placeholder="Direccion" class="form-control">
      </div>
      <div class="col-lg-6">
          <label class="lead" for="ciudad">Ciudad</label>
          <input type="text" name="ciudad"  placeholder="Ciudad" class="form-control">
      </div>
      <div class="col-lg-6">
          <label class="lead" for="provincia">Provincia</label><br>
          <select name="provincia">
            <option value="" selected disabled>Seleccione la Provincia</option>
            <option value='Álava'>Álava</option>
            <option value='Albacete'>Albacete</option>
            <option value='Alicante'>Alicante</option>
            <option value='Almería'>Almería</option>
            <option value='Asturias'>Asturias</option>
            <option value='Ávila'>Ávila</option>
            <option value='Badajoz'>Badajoz</option>
            <option value='Barcelona'>Barcelona</option>
            <option value='Burgos'>Burgos</option>
            <option value='Cáceres'>Cáceres</option>
            <option value='Cádiz'>Cádiz</option>
            <option value='Cantabria'>Cantabria</option>
            <option value='Castellón'>Castellón</option>
            <option value='Ceuta'>Ceuta</option>
            <option value='Ciudad Real'>Ciudad Real</option>
            <option value='Córdoba'>Córdoba</option>
            <option value='Cuenca'>Cuenca</option>
            <option value='Girona'>Girona</option>
            <option value='Las Palmas'>Las Palmas</option>
            <option value='Granada'>Granada</option>
            <option value='Guadalajara'>Guadalajara</option>
            <option value='Guipúzcoa'>Guipúzcoa</option>
            <option value='Huelva'>Huelva</option>
            <option value='Huesca'>Huesca</option>
            <option value='Illes Balears'>Illes Balears</option>
            <option value='Jaén'>Jaén</option>
            <option value='A Coruña'>A Coruña</option>
            <option value='La Rioja'>La Rioja</option>
            <option value='León'>León</option>
            <option value='Lleida'>Lleida</option>
            <option value='Lugo'>Lugo</option>
            <option value='Madrid'>Madrid</option>
            <option value='Málaga'>Málaga</option>
            <option value='Melilla'>Melilla</option>
            <option value='Murcia'>Murcia</option>
            <option value='Navarra'>Navarra</option>
            <option value='Ourense'>Ourense</option>
            <option value='Palencia'>Palencia</option>
            <option value='Pontevedra'>Pontevedra</option>
            <option value='Salamanca'>Salamanca</option>
            <option value='Segovia'>Segovia</option>
            <option value='Sevilla'>Sevilla</option>
            <option value='Soria'>Soria</option>
            <option value='Tarragona'>Tarragona</option>
            <option value='Santa Cruz de Tenerife'>Santa Cruz de Tenerife</option>
            <option value='Teruel'>Teruel</option>
            <option value='Toledo'>Toledo</option>
            <option value='Valencia/Valéncia'>Valencia/Valéncia</option>
            <option value='Valladolid'>Valladolid</option>
            <option value='Vizcaya'>Vizcaya</option>
            <option value='Zamora'>Zamora</option>
            <option value='Zaragoza'>Zaragoza</option>
          </select>
      </div>
      <div class="col-lg-6">
          <label class="lead" for="CP">Codigo Postal</label>
          <input type="number" name="CP" min="01000" max="52999" placeholder="Codigo Postal" class="form-control">
      </div>
      <div class="col-lg-6">
          <label class="lead" for="telefono">Teléfono</label>
          <input type="tel" name="telefono"  placeholder="telefono" class="form-control">
      </div>

    <div style="margin-left:20%;padding-bottom:4%">
      <button class="btn btn-primary" type="submit" name="enviar">Guardar Datos de Perfil</button>
    </div>
  </form>
</div>
</div>

@endsection

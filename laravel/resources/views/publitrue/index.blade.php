@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Articulos Trueque

@endsection

@section('contenido')

@if (isset($trueque))
@foreach ($trueque->chunk(3) as $trueq)
<div class="row">
  @foreach($trueq as $tru)
  @if ($tru->flag==1)
  <div class="col-md-4">
    <div class="card border-primary mb-3 mx-auto" style="max-width: 20rem;">
      <div class="card-header"><h3 class="text-center">{{$tru->nombre}}</h3></div>
      <div class="card-body">
        <div class="thumbnail">
          <img class="mx-auto d-block img-thumbnail" width="242px" height="auto" src="img/{{$tru->img}}"/>
          <div class="caption">
            <h3 class="text-center">Descripción</h3>
            <p>{{$tru->descripcion}}</p>
            <h3 class="text-center">Busco a Cambio</h3>
            <p>{{$tru->artbuscados}}</p>
            <p><a href="{{route('publitrue.edit',$tru->id)}}" class="btn btn-primary" role="button">Ver Mas</a> <a href="{{route('cambio.edit',$tru->id)}}" class="btn btn-outline-success" role="button">Ofrecer Cambio</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif
  @endforeach
</div>
@endforeach
@endif

@endsection

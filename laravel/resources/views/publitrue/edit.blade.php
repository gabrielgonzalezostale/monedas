@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Articulo Trueque

@endsection

@section('contenido')


<style type="text/css">

.zoom {
-webkit-transition: all 0.35s ease-in-out;
-moz-transition: all 0.35s ease-in-out;
transition: all 0.35s ease-in-out;
cursor: -webkit-zoom-in;
cursor: -moz-zoom-in;
cursor: zoom-in;
}

.zoom:hover,
.zoom:active,
.zoom:focus {
-ms-transform: scale(2);
-moz-transform: scale(2);
-webkit-transform: scale(2);
-o-transform: scale(2);
transform: scale(2);
position:relative;
z-index:100;
}

.DivName {cursor: pointer}
}

</style>

<div class="container mt-5">
  <div class="row">
    <div class="col-6">
      <img class="d-block img-thumbnail zoom DivName" max-width: "100%" height="auto" src="../../img/{{$trueque->img}}"/>
      <h5>{{$trueque->descripcion}}</h5>
    </div>
    <div class="col-6">
      <div class="">
        <h3>{{$trueque->nombre}}</h3>
      </div>
      <div class="mt-4">
        <h4>Articulos Buscados </h4>
        <p>{{$trueque->artbuscados}}</p>
      </div>
      <div class="mt-4">
        <h4 class="d-inline">Condición: </h4><p class="d-inline"> {{$trueque->condicion}}</p>
      </div>
      <div class="mt-4">
        <h4 class="d-inline">Época: </h4><p class="d-inline">{{$trueque->epoca}}</p>
      </div>
      <div class="mt-4">
        <h4 class="d-inline">Año: </h4><p class="d-inline">{{$trueque->agno}}</p>
      </div>
      <div class="mt-4">
        <h4 class="d-inline">Nacionalidad: </h4><p class="d-inline">{{$trueque->nacionalidad}}</p>
      </div>
      <div class="mt-4">
        <h4 class="d-inline">Tipo de Articulo: </h4><p class="d-inline">{{$trueque->tipo}}</p>
      </div>
      <div class="mt-4">
        <a href="{{route('cambio.edit',$trueque->id)}}" class="btn btn-primary btn-lg" role="button">Ofrecer Cambio</a>
      </div>
    </div>
  </div>

@endsection

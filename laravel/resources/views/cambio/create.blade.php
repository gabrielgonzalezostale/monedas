  @extends('layouts.plantilla')

  @section('menu')

  @endsection

  @section('titulo')

  Ofrecer un cambio por el Articulo

  @endsection

  @section('contenido')

  <div class="row">
    <div class="col-md-6">

  <table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Datos de Contacto del Anunciante:</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Anunciante:</td>
      <td>{{$usuario->name}}</td>
    </tr>
    <tr>
      <td>Email:</td>
      <td>{{$usuario->email}}</td>
    </tr>
  </tbody>
  </table>

  <table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Resumen del Articulo:</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Nombre:</td>
      <td>{{$cambio->nombre}}</td>
    </tr>
    <tr>
      <td>Condición:</td>
      <td>{{$cambio->condicion}}</td>
    </tr>
    <tr>
      <td>Articulos Buscados:</td>
      <td>{{$cambio->artbuscados}}</td>
    </tr>

  </tbody>
  </table>
  </div>

  <div class="col-md-6">

                <div class="well well-sm">
                        <fieldset>
                            <legend class="text-center header">Contactar con el Anunciante</legend>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                                <div class="col-md-8">
                                    <input id="fname" name="name" type="text" placeholder="Nombre" value="{{$user->name}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                                <div class="col-md-8">
                                    <input id="email" name="email" type="text" placeholder="Email" value="{{$user->email}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                                <div class="col-md-8">
                                    <input id="phone" name="phone" type="number" min="600000000" placeholder="Teléfono" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                                <div class="col-md-8">
                                    <textarea class="form-control" id="message" name="message" placeholder="Escribe tu mensaje u oferta para el anunciante aquí." rows="7"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <a href="{{route('cambio.index')}}"><button class="btn btn-primary btn-lg">Enviar</button></a>
                                </div>
                            </div>
                        </fieldset>
              </div>
          </div>

  </div>

  @endsection

@extends('layouts.plantilla')

@section('menu')

@endsection

@section('titulo')

Articulos Trueque

@endsection

@section('contenido')

<a href="{{route('trueque.create')}}"><button class="btn btn-success">Crear Nuevo Articulo Para Trueque</button></a>

<table class="table">
    <tr class="table-primary">
      <th scope="col">Nombre</th>
      <th scope="col">Tipo</th>
      <th scope="col">Descripción</th>
      <th scope="col">Condición</th>
      <th scope="col">Época</th>
      <th scope="col">Año</th>
      <th scope="col">Nacionalidad</th>
      <th scope="col">Articulos Buscados</th>
      <th scope="col">Imagen</th>
      <th scope="col">Estado publicacion</th>
      <th scope="col"></th>
    </tr>
    @if (isset($trueque))
    @foreach ($trueque as $tru)
    <tr class="table-secondary">
      <td>{{$tru->nombre}}</td>
      <td>{{$tru->tipo}}</td>
      <td>{{$tru->descripcion}}</td>
      <td>{{$tru->condicion}}</td>
      <td>{{$tru->epoca}}</td>
      <td>{{$tru->agno}}</td>
      <td>{{$tru->nacionalidad}}</td>
      <td>{{$tru->artbuscados}}</td>
      <td><img width="200px" height="auto" src="img/{{$tru->img}}"/></td>
      @if ($tru->flag == 1)
      <td>Publicado</td>
      @else
      <td>No publicado</td>
      @endif
      <td>
      <form action="/trueque/{{$tru->id}}" method="post">
        @csrf
        @method('DELETE')
        <button type="submit" name="enviar" class="btn btn-danger">Eliminar Articulo</button>
      </form>
      </td>
    </tr>
    @endforeach
    @endif
</table>

@endsection

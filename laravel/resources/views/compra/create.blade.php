  @extends('layouts.plantilla')

  @section('menu')

  @endsection

  @section('titulo')

  Procedimiento de la Compra

  @endsection

  @section('contenido')

<style media="text/css">
.credit-card-div  span { padding-top:10px; }
.credit-card-div img { padding-top:30px; }
.credit-card-div .small-font { font-size:9px; }
.credit-card-div .pad-adjust { padding-top:10px; }
</style>

<div class="row">
  <div class="col-md-6">
<table class="table">
<thead class="thead-light">
  <tr>
    <th scope="col">Resumen del Pedido:</th>
    <th scope="col"></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Vendedor:</td>
    <td>{{$usuario->name}}</td>
  </tr>
  <tr>
    <td>Nombre:</td>
    <td>{{$compra->nombre}}</td>
  </tr>
  <tr>
    <td>Condición:</td>
    <td>{{$compra->condicion}}</td>
  </tr>
  <tr>
    <td><h3>Precio:</h3></td>
    <td><h3>{{$compra->precio}} €</h3></td>
  </tr>
</tbody>
</table>
</div>

<div class="col-md-6">
<div class="panel panel-default" >
 <div class="panel-heading">

<div class="col-md-12 jumbotron">
      <div class="row ">
              <div class="col-md-12">
                <span class="help-block text-muted small-font" > Introducir número de tarjeta</span>
                  <input type="number" min="4000000000000" max="9999999999999999" class="form-control" placeholder="0000-0000-0000-0000" />
              </div>
          </div>
     <div class="row ">
              <div class="col-md-3 col-sm-3 col-xs-3 mt-3">
                  <span class="help-block text-muted small-font" > Mes de expiración</span>
                  <input type="number" min="1" max="12" class="form-control" placeholder="MM" />
              </div>
         <div class="col-md-3 col-sm-3 col-xs-3 mt-3">
                  <span class="help-block text-muted small-font" >  Año de expiración</span>
                  <input type="number" min="21" max="99" class="form-control" placeholder="YY" />
              </div>
        <div class="col-md-3 col-sm-3 col-xs-3 mt-3">
                  <span class="help-block text-muted small-font" >  CCV</span>
                  <input type="number" min="000" max="999" class="form-control" placeholder="CCV" />
              </div>
         <div class="col-md-3 col-sm-3 col-xs-3 mt-4">
<img src="../../img/1.png" class="img-rounded" width="50px" height="auto" />
         </div>
          </div>
     <div class="row ">
              <div class="col-md-12 pad-adjust mt-3">
                  <span class="help-block text-muted small-font" > Nombre en la Tarjeta</span>
                  <input type="text" class="form-control" placeholder="Nombre en la Tarjeta" />
              </div>
          </div>
     <div class="row">
<div class="col-md-12 pad-adjust mt-3">
    <div class="checkbox">
    <label>
      <input type="checkbox" checked class="text-muted"> Guardar datos para futuras compras
    </label>
  </div>
</div>
     </div>
       <div class="row ">
            <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust mt-3">
              <a href="../home"><button class="btn btn-danger">Cancelar</button></a>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust mt-3">
                  <a href="{{route('compra.index')}}"><button class="btn btn-warning btn-block">Pagar ahora</button></a>
              </div>
          </div>

                   </div>
              </div>
</div>
</div>
</div>

  @endsection

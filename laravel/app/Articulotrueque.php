<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulotrueque extends Model
{
    //

    protected $fillable=[

      "user_id",
      "nombre",
      "tipo",
      "descripcion",
      "condicion",
      "epoca",
      "agno",
      "nacionalidad",
      "artbuscados",
      "img",
      "flag",

    ];

    public function user(){
      return $this->belongsTo('App\User');
    }

}

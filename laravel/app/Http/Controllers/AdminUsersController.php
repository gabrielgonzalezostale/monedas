<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Dato;

class AdminUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Muestra la lista de Usuarios

        $users=User::all();

        return view("admin.users.index", compact("users"));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("admin.users.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Comprobar que los valores del formulario sean los correctos
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        //Para insertar nuevos Usuarios
        $users=new User;

        //le digo a que corresponde cada cosa de la BBDD de los input del form
        $users->name=$request->name;
        $users->email=$request->email;
        $users->password=$request->password;
        $users->role_id=$request->rol;

        $users->save();

        //1-el formulario se envia a esta funcion
        //2-creamos el objeto del modelo
        //3-asignamos cada registro con su valor del formulario
        //4-guardamos (insertamos) la nueva columna
        //5-redirigimos hasta la pagina de gestion de usuarios
        $users=User::all();

        return view("admin.users.index", compact("users"));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //Le pasamos como parametro el id de un usuario, y nos enviara a la pagina de ese usuario para poder actualizarlo
      $users=User::findOrFail($id);
      $datos=$users->dato;
      return view("admin.users.edit", compact("datos","users"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      //Recogemos la informacion del formulario de actualizacion, actualizamos y redireccionamos al index
      $users=User::findOrFail($id);

      //Comprobar que los valores del formulario sean los correctos
      $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required', 'string', 'email', 'max:255'],
      ]);

      //le digo a que corresponde cada cosa de la BBDD de los input del form
      $users->name=$request->name;
      $users->email=$request->email;
      $users->role_id=$request->rol;

      $users->update($request->all());

      //vuelvo a la vista index
      $users=User::all();

      return view("admin.users.index", compact("users"));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //Para eliminar usuarios de la BBDD

      $users=User::findOrFail($id);

      $users->delete();

      //vuelvo a la vista index
      $users=User::all();

      return view("admin.users.index", compact("users"));
    }
}

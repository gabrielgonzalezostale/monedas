<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Dato;
use App\User;
use App\Articuloventa;

class ArticuloventasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users=Auth::user();
        $usuario=User::findOrFail($users->id);
        $venta=$usuario->venta;

        if($venta){
            return view("venta.index", compact("venta"));
        }else{
            return view("venta.index");
    }
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('venta.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $entrada=$request->all();

        if($archivo=$request->file('img')){

          $nombre=$archivo->getClientOriginalName();
          $archivo->move('img',$nombre);
          $entrada['img']=$nombre;
          $entrada['user_id']=Auth::id();

        }

        Articuloventa::create($entrada);

        //una vez se ha creado volvemos al index
        $users=Auth::user();
        $usuario=User::findOrFail($users->id);
        $venta=$usuario->venta;

        if($venta){
            return view("venta.index", compact("venta"));
        }else{
            return view("venta.index");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $art=Articuloventa::findOrFail($id);

        $art->delete();

        //una vez se ha borrado volvemos al index
        $users=Auth::user();
        $usuario=User::findOrFail($users->id);
        $venta=$usuario->venta;

        if($venta){
            return view("venta.index", compact("venta"));
        }else{
            return view("venta.index");
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         //Para mostrar los animales existentes

         $clientes=Cliente::all();

         return view("clientes.index", compact("clientes"));

     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
         //Nos muestra el formulario para crear animales
         return view('clientes.create');
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {

         //Para insertar nuevos animales en la tienda

         $clientes=new Cliente;
         //le digo a que corresponde cada cosa de la BBDD de los input del form
         $clientes->Nombre=$request->Nombre;
         $clientes->Apellidos=$request->Apellidos;
         $clientes->DNI=$request->DNI;
         $clientes->Telefono=$request->Telefono;
         $clientes->Direccion=$request->Direccion;
         $clientes->Correo=$request->Correo;

         $clientes->save();

         //1-el formulario se envia a esta funcion
         //2-creamos el objeto del modelo
         //3-asignamos cada registro con su valor del formulario
         //4-guardamos (insertamos) la nueva columna
         //5-redirigimos hasta la pagina de lectura de animales para ver los cambios
         return redirect("/clientes");

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
         //Le pasamos como parametro el id de un animal, y nos enviara a la pagina de ese animal para poder actualizarlo
         $clientes=Cliente::findOrFail($id);

         return view("clientes.edit", compact("clientes"));

     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
         //Recogemos la informacion del formulario de actualizacion, actualizamos y redireccionamos al index
         $clientes=Cliente::findOrFail($id);

         $clientes->Nombre=$request->Nombre;
         $clientes->Apellidos=$request->Apellidos;
         $clientes->DNI=$request->DNI;
         $clientes->Telefono=$request->Telefono;
         $clientes->Direccion=$request->Direccion;
         $clientes->Correo=$request->Correo;

         $clientes->update($request->all());

         return redirect("/clientes");
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         //Para eliminar clientes de la BBDD

         $clientes=Cliente::findOrFail($id);

         $clientes->delete();

         return redirect("/clientes");
     }
}

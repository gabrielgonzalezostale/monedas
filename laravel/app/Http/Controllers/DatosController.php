<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Dato;
use App\User;

class DatosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users=Auth::user();
        //si no tiene datos paso a la vista de crearlos, sino al index
        $user=User::findOrFail($users->id);
        $datos=$user->dato;
        if($datos){
            return view("perfil.index", compact("datos","users"));
        }else{
            return view("perfil.create", compact("users"));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ////Para insertar nuevos Datos del perfil de usuario
        $datos=new Dato;

        $datos->user_id=Auth::id();
        $datos->nombre=$request->nombre;
        $datos->apellido1=$request->apellido1;
        $datos->apellido2=$request->apellido2;
        $datos->direccion=$request->direccion;
        $datos->ciudad=$request->ciudad;
        $datos->provincia=$request->provincia;
        $datos->CP=$request->CP;
        $datos->telefono=$request->telefono;

        $datos->save();

        //para volver al index
        $users=Auth::user();
        $user=User::findOrFail($users->id);
        $datos=$user->dato;

        return view("perfil.index", compact("datos","users"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Vista para editar el perfil de usuario
        $users=User::findOrFail($id);
        $datos=$users->dato;

        return view("perfil.edit", compact("datos","users"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datos=Dato::findOrFail($id);

        $datos->nombre=$request->nombre;
        $datos->apellido1=$request->apellido1;
        $datos->apellido2=$request->apellido2;
        $datos->direccion=$request->direccion;
        $datos->ciudad=$request->ciudad;
        $datos->provincia=$request->provincia;
        $datos->CP=$request->CP;
        $datos->telefono=$request->telefono;

        $datos->update($request->all());

        if(Auth::user()->EsAdmin()){
          //si es admin le llevara a su index
          $users=User::all();
          return view("admin.users.index", compact("users"));
        }else{
        //para volver al index de usuario
        $users=Auth::user();
        $user=User::findOrFail($users->id);
        $datos=$user->dato;

        return view("perfil.index", compact("datos","users"));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //
    }
}

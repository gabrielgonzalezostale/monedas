<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ModeradorController extends Controller
{
    //

    public function __construct(){

      $this->middleware('EsMod');

    }

    public function index(){

      return "Tienes rol de moderador";

    }

}

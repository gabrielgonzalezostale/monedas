<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    //

    public function __construct(){

      $this->middleware('EsUser');

    }

    public function index(){

      return "Tienes rol de usuario";

    }

}

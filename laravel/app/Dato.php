<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dato extends Model
{
    //
    protected $fillable=[

      "nombre",
      "apellido1",
      "apellido2",
      "direccion",
      "ciudad",
      "provincia",
      "CP",
      "telefono",
      "img",

    ];

    public function user(){
      return $this->belongsTo('App\User');
    }

}

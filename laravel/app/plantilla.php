<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
  protected $fillable=[

    "Nombre",
    "Apellidos",
    "DNI",
    "Telefono",
    "Direccion",
    "Correo",


  ];
  public function comments(){
    return $this->hasMany('App\Historial_producto');
    return $this->hasMany('App\Historial_animale');
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articuloventa extends Model
{
    //

    protected $fillable=[

      "user_id",
      "nombre",
      "tipo",
      "descripcion",
      "condicion",
      "epoca",
      "agno",
      "nacionalidad",
      "precio",
      "img",
      "flag",

    ];

    public function user(){
      return $this->belongsTo('App\User');
    }

}

<?php

/*use App\Historial_producto;
use App\Historial_animale;
use App\Cliente;*/

//Pagina principal de la web
/*Route::get('/', function () {
    return view('index');
});*/

Route::get('/','PrincipalController@index');

Route::get('/home', 'HomeController@index')->name('home');

//Rutas para registro/logeo... usuarios
Auth::routes();

//Para que el administrador Agrege/modifique/borre usuarios usa el middleware para que solo puedan entrar administradores
Route::resource("/admin/users","AdminUsersController")->middleware('EsAdmin');

//Para que los usuarios puedan ver/modificar su Perfil
Route::resource("/perfil","DatosController")->middleware('auth');

//Para que los usuarios puedan ver/crear y eliminar articulos para trueque
Route::resource("/trueque","ArticulotruequesController")->middleware('auth');

//Para que los usuarios puedan ver/crear y eliminar articulos para vender
Route::resource("/venta","ArticuloventasController")->middleware('auth');

//Para que el moderador publique articulos de trueque
Route::resource("/moder/true","ModerPubliController")->middleware('EsMod');
//Para que el moderador publique articulos de venta
Route::resource("/moder/vent","ModerPubliVenController")->middleware('EsMod');

//Para que todos vean los Articulos de trueque
Route::resource("/publitrue","PublitrueController");
//Para que todos vean los Articulos para Comprar
Route::resource("/publivent","PubliventController");

//Para que los usuarios puedan comprar articulos
Route::resource("/compra","ComprasController")->middleware('auth');

//Para que los usuarios puedan ofrecer articulos para cambiarlos
Route::resource("/cambio","CambioController")->middleware('auth');



//Para redireccion con la lista de rutas propia de laravel

/*Route::resource("/animales","AnimalesController")->middleware('auth');

Route::resource("/productos","ProductosController")->middleware('auth');

Route::resource("/clientes","ClientesController")->middleware('auth');

Route::resource("/historial_productos","Historial_ProductoController")->middleware('auth')->middleware('EsAdmin');

Route::get('/comprado', function () {
    return view('historial_productos.comprado');
})->middleware('auth')->middleware('EsAdmin');

Route::get('/nocomprado', function () {
    return view('historial_productos.nocomprado');
})->middleware('auth')->middleware('EsAdmin');

Route::resource("/historial_animales","Historial_AnimaleController")->middleware('auth')->middleware('EsAdmin');

Route::get('/compradoa', function () {
    return view('historial_animales.comprado');
})->middleware('auth')->middleware('EsAdmin');

Route::get('/nocompradoa', function () {
    return view('historial_animales.nocomprado');
})->middleware('auth')->middleware('EsAdmin');


*/
//para usuarios no registrados
//->middleware('auth');
//->middleware('EsUser');
//->middleware('EsAdmin');




/*Route::post('/buscar','BusquedaController@buscar');*/

//PRUEBA DE SUBCONSULTAS
/*Route::get('/lorem', function () {
  //  $comments = App\Producto::find(1)->comments;

    $comments = App\Cliente::where("Nombre","Chaim")->first()->comments;

    foreach ($cliente as $key){
      echo $key->Precio;
      echo $key->stock;
      echo $key->cliente_id;
      echo $key->producto_id;
    }

});*/

/*Route::get("animales","AnimalesController@index");

Route::post("animales","AnimalesController@store");

Route::get("animales/create","AnimalesController@create");

Route::put("animales/{animale}","AnimalesController@update");

Route::delete("animales/{animale}","AnimalesController@destroy");

Route::get("animales/{animale}/edit","AnimalesController@edit");*/

//Paginas de animales
/*Route::get("crearanimales","AnimalesController@create");
Route::get("leeranimales","AnimalesController@index");*/

/*
Route::get('/lorem', function () {
    return ('lo que tu quieras que salga');
});

Route::get('usuario','ControllerUsuario@index');
Route::get('saboteador','ControllerUsuario@sabotearUsuario');

//rutas ej migracion
Route::get('ruta66','ControllerAlumno@ruta66');

Route::get('area51','ControllerProfesor@random');

Route::get('/palabra', function () {
    return ('lo que tu quieras que salga');

//Ejercicio 21 ene 2020

Route::resource('products2','Product2Controller');

});*/
